## Required components

1. Timer:
    * NE555P
2. Resistors:
    * 2x 1K Ω
3. Capacitors:
    * 2x 100nF
4. Diodes:
    * 3x 1N4004
5. Potentiometer:
    * P160KN-0QC15B100K	
6. Transistor:
    * TIP120
7. Terminal Blocks - Wire to Board:
    * 2x 282837-2